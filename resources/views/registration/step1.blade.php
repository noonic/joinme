@extends('master')

@section('content')
<div class="col-lg-12 col-md-6 col-xs-12 row text-center">
  <div class="col-lg-4 col-md-5"> </div>
   <div class="col-lg-4 col-md-5 col-xs-12">
		<img src="{{ asset('/img/logo_static.png') }}" alt="">
	</div>
	<div class="col-lg-4 col-md-5 col-xs-5"> </div>
</div>
<div class="">
    <div class="col-md-12 row">
    <div class="col-lg-2 col-md-6"><p></p> </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 logWrap">
            <a href="{{ url('fbAuth') }}">
                <p class="fbBtn">
                    <i class="fa fa-facebook-square"></i>&nbsp;Accedi con Facebook
                </p>
            </a>
        </div>
        {!! Form::open(['url' => 'storeStep1']) !!}
        @if ($errors->has('email'))
        <?php $class = 'has-error'; ?>
        @else
        <?php $class = null; ?>
        @endif
        @if ($errors->has('agree'))
        <?php $red = 'red'; ?>
        @else
        <?php $red = null; ?>
        @endif
       <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 logWrap">
        <div class="mailBtn">
            <div class="input-group {{ $class }}">
                {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Inserisci tuo indirizzo e-mail']) !!}
                <span class="input-group-btn">
                    {!! Form::submit('Avanti', ['class' => 'btn btn-danger']) !!}
                </span>
            </div>
            <label>
                {!! Form::checkbox('agree') !!}
                <span class="{{ $red }}">consento per la privacy</span>
            </label>
            @if ($errors->any()) 
            @foreach($errors->all() as $error)
            <span class="error">* {{ $error }}</span>
            @endforeach
            @endif
        </div>
        </div>
    </div>
</div>

@stop