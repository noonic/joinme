@extends('master')

@section('content')


<div class="detForms col-lg-6 col-lg-offset-3">
    @if ($errors->has('firstname'))
        <?php $firstNameError = 'req'; ?>
    @else
        <?php $firstNameError = ''; ?>
    @endif
    @if ($errors->has('lastname'))
        <?php $lastNameError = 'req'; ?>
    @else
        <?php $lastNameError = ''; ?>
    @endif
    @if ($errors->has('email'))
        <?php $emailError = 'req'; ?>
    @else
        <?php $emailError = ''; ?>
    @endif
    @if ($errors->has('phone'))
        <?php $phoneError = 'req'; ?>
    @else
        <?php $phoneError = ''; ?>
    @endif
    @if ($errors->has('age'))
        <?php $ageError = 'req'; ?>
    @else
        <?php $ageError = ''; ?>
    @endif
    @if ($errors->has('school'))
        <?php $schoolError = 'req'; ?>
    @else
        <?php $schoolError = ''; ?>
    @endif
    @if ($errors->has('interests'))
        <?php $interestsError = 'req'; ?>
    @else
        <?php $interestsError = ''; ?>
    @endif

    {!! Form::open(['url' => 'storeStep2']) !!}

    {!! Form::label('firstname', 'First Name') !!}
    {!! Form::text('firstname', $firstname, ['class' => $firstNameError]) !!}

    {!! Form::label('lastname', 'Last Name') !!}
    {!! Form::text('lastname', $lastname, ['class' => $lastNameError]) !!}

    {!! Form::label('email', 'Email') !!}
    {!! Form::text('email', $email, ['class' => $emailError]) !!}

    {!! Form::label('phone', 'Telefono') !!}
    {!! Form::text('phone', $phone, ['class' => $phoneError]) !!}
    
    {!! Form::label('age', 'Age') !!}
    {!! Form::text('age', $age, ['class' => $ageError]) !!}

    {!! Form::label('school', 'School') !!}
    {!! Form::text('school', $school, ['class' => $schoolError]) !!}

    {!! Form::label('interests', 'Interests') !!}
    {!! Form::textarea('interests', $interests, ['class' => $interestsError]) !!}

    {!! Form::hidden('participantId', $id) !!}

    {!! Form::submit('Next', ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
</div>


@stop