@extends('master')

@section('content')


<div class="logWrap">
    <div class="row">
        <div  class="col-lg-6 col-lg-offset-3">
            <div class="row">
                {!! Form::open(['url' => 'storeStep3']) !!}                
                <div class="col-lg-10 text-center">
                    <div class="custSelect"> 
                        <i class="fa fa-sort-desc"></i>
                        @if ($errors->has('campo'))
                            <?php $class = "req"; ?>
                        @else
                            <?php $class = "" ?>
                        @endif
                        {!! Form::select('campo', $campoList, null, ['class' => $class]) !!} 
                    </div>
                </div>
                {!! Form::hidden('participantId', $id) !!}
                <div class="col-lg-2 mailBtn">
                    {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}

            </div>
        </div>

    </div>
</div>

<!--
{!! Form::open(['url' => 'storeStep3']) !!}
{!! Form::select('campo', $campoList, null) !!}
{!! Form::hidden('participantId', $id) !!}
{!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
{!! Form::close() !!}-->

@stop