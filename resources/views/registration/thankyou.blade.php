@extends('master')

@section('content')

<div class="logWrap">
    <div class="row">
        <div  class="col-lg-6 col-lg-offset-3">
            <h1 class="thankuTxt">Thank you!<a href="{{ url('/') }}">go back</a></h1>
        </div>

    </div>
</div>

@stop