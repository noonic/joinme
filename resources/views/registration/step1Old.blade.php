@extends('master')

@section('content')

<div class="logWrap">
    <div class="row">
        <div class="col-lg-6 text-center">
            <a href="{{ url('fbAuth') }}">
                <span class="fbBtn">
                    <i class="fa fa-facebook-square"></i>&nbsp;Login with Facebook
                </span>
            </a>
        </div>
        {!! Form::open(['url' => 'storeStep1']) !!}
        @if ($errors->has('email'))
        <?php $class = 'has-error'; ?>
        @else
        <?php $class = null; ?>
        @endif
        @if ($errors->has('agree'))
        <?php $red = 'red'; ?>
        @else
        <?php $red = null; ?>
        @endif

        <div class="col-lg-6 mailBtn">
            <div class="input-group {{ $class }}">
                {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter your email address']) !!}
                <span class="input-group-btn">
                    {!! Form::submit('Next', ['class' => 'btn btn-danger']) !!}
                </span>
            </div>
            <label>
                {!! Form::checkbox('agree') !!}
                <span class="{{ $red }}">I accept the privacy policy</span>
            </label>
            @if ($errors->any()) 
            @foreach($errors->all() as $error)
            <span class="error">* {{ $error }}</span>
            @endforeach
            @endif
        </div>
    </div>
</div>

@stop