<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');

//Route::get('home', 'HomeController@index');

//Route::controllers([
//	'auth' => 'Auth\AuthController',
//	'password' => 'Auth\PasswordController',
//]);

Route::get('/', 'RegistrationController@step1');
Route::post('storeStep1', 'RegistrationController@storeStep1');

Route::get('step2', 'RegistrationController@step2');
Route::post('storeStep2', 'RegistrationController@storeStep2');

Route::get('step3', 'RegistrationController@step3');
Route::post('storeStep3', 'RegistrationController@storeStep3');

Route::get('thank-you', 'RegistrationController@thankyou');

Route::get('fbAuth', 'FacebookController@fbAuth');

Route::get('/{anything_other_than_above}', function() {
    return redirect('/');
});
