<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use App\Participants;

class RegistrationController extends Controller {

    public function step1() {
        return view('registration.step1');
    }
    
    public function storeStep1(\Illuminate\Http\Request $request) {
        $rules = array(
            'email' => 'required|email|unique:participants,email',
            'agree' => 'accepted'
        );
        $this->validate($request, $rules);
        $input = Request::all();
        $participant = Participants::updateOrCreate(array('email' => $input['email']), $input);
        session()->regenerate();
        session(['participantId' => $participant->id, 'participantEmail' => $participant->email]);
        return redirect('step2');
    }

    public function step2(\Illuminate\Http\Request $request) {
        $participantId = session('participantId');
        $participantEmail = session('participantEmail');
        $participantPhone = session('participantPhone');
        $participantFirstname = session('participantFirstname');
        $participantLastname = session('participantLastname');
        $participantAge = session('participantAge');
        $participantSchool = session('participantSchool');
        $participantInterests = session('participantInterests');
        if ( $participantId === null ) {
            return redirect('/');
        }
        return view(
                'registration.step2', 
                array(
                    'id' => $participantId, 
                    'email' => $participantEmail,
                    'phone' => $participantPhone,
                    'firstname' => $participantFirstname,
                    'lastname' => $participantLastname,
                    'age' => $participantAge,
                    'school' => $participantSchool,
                    'interests' => $participantInterests,
                )
        );
    }
    
    public function storeStep2(\Illuminate\Http\Request $request) {
        $rules = array(
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'age' => 'required',
            'school' => 'required',
            'interests' => 'required',
        );
        $this->validate($request, $rules);
        $input = Request::all();
        Participants::updateOrCreate(array('id' => $input['participantId']), $input);
        return redirect('step3');
    }

    public function step3(\Illuminate\Http\Request $request) {
        $participantId = session('participantId');
        if ( $participantId === null ) {
            return redirect('/');
        }
        $campoList = array(
            0 => 'Select',
            'Tecnico' => 'Tecnico',
            'Accademico' => 'Accademico',
            'Fisico' => 'Fisico'
        );
        return view('registration.step3', array('campoList' => $campoList, 'id' => $participantId));
    }

    public function storeStep3(\Illuminate\Http\Request $request) {
        $rules = array(
            'campo' => 'required|not_in:0'
        );
        $this->validate($request, $rules);
        $input = Request::all();
        Participants::updateOrCreate(array('id' => $input['participantId']), $input);
        session()->forget('participantId');
        return redirect('thank-you');
    }

    public function thankyou() {
        session()->flush();
        return view('registration.thankyou');
    }

}
