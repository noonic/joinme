<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Participants;

class FacebookController extends Controller {

    public function fbAuth() {
        if (\Input::has('code')) {
            $user = Socialite::with('facebook')->user();
            $data = array(
                'email' => $user->email,
                'firstname' => $user->user['first_name'],
                'lastname' => $user->user['last_name'],
                'fbId' => $user->id
            );
            $participant = Participants::updateOrCreate(array('email' => $user->email), $data);
            session([
                'participantId' => $participant->id,
                'participantEmail' => $participant->email,
                'participantPhone' => $participant->phone,
                'participantFirstname' => $participant->firstname,
                'participantLastname' => $participant->lastname,
                'participantAge' => $participant->age,
                'participantSchool' => $participant->school,
                'participantInterests' => $participant->interests,
            ]);
            return redirect('step2');
        }
//        return Socialite::with('facebook')->scopes(['user_interests', 'user_education_history'])->redirect();
        return Socialite::with('facebook')->redirect();
    }

}
