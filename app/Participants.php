<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Participants extends Model {

    protected $fillable = [
        'firstname',
        'lastname',
        'age',
        'school',
        'interests',
        'email',
        'phone',
        'campo',
        'fbId'
    ];

}
